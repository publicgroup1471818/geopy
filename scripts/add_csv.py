import os
import pandas as pd

from django.conf import settings

from src.models.location import Location


def run():
    print('Загружается csv файл... пожалуйста подождите...')
    file_path = os.path.join(settings.BASE_DIR, 'cszips.csv')
    with open(file_path) as file:
        reader = pd.read_csv(file)

        for _, row in reader.iterrows():
            new_location = Location(
               zip = row['zip'],
               city = row['city'],
               state_id = row['state_id'],
               state_name = row['state_name'],
               lat = row['lat'],
               lng = row['lng'],
            )
            new_location.save()

