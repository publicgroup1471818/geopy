from src.models import Location
from src.services.services import create_random_trucks


def run():
    locations_zips = [loc.zip for loc in Location.objects.all()]
    create_random_trucks(20, locations_zips)
