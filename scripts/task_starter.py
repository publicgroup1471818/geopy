from django.core.exceptions import ValidationError
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from django_celery_beat.utils import timezone


def run() -> None:
    interval = IntervalSchedule.objects.create(every=3, period='minutes')
    interval.save()

    try:
        PeriodicTask.objects.create(
            name='Truck location changer',
            task='change_all_trucks_location',
            interval=interval,
            start_time=timezone.now(),
        )
    except ValidationError:
        pass
