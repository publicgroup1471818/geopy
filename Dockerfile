FROM python:3.11

RUN mkdir /geo 
RUN mkdir /geo/static 

COPY poetry.lock pyproject.toml /geo/

ENV PYTHONPATH=${PYTHONPATH}:${PWD} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.4.0

WORKDIR /geo

RUN pip install "poetry==$POETRY_VERSION"
RUN poetry config virtualenvs.create false \
  && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

COPY . /geo

EXPOSE 8000

RUN adduser --disabled-password docker-admin

USER docker-admin 
