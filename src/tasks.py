from random import choice

from celery import shared_task

from src.models import Truck, Location


@shared_task
def change_truck_location(cur_truck: Truck, locations_zips: list) -> None:
    """Изменение локации машины на рандомную."""
    cur_truck.location = Location.objects.get(pk=choice(locations_zips))
    cur_truck.save()


@shared_task(name='change_all_trucks_location')
def all_trucks_location() -> None:
    """Изменение локаций машин."""
    all_trucks = Truck.objects.all().only('location')
    locations_zips = [loc.zip for loc in Location.objects.all()]
    for truck in all_trucks:
        change_truck_location(truck, locations_zips)


