from rest_framework import serializers
from django.core.cache import cache

from src.models import Truck, Shipment, Location
from src.services.services import get_distance_trucks


class TruckSerializer(serializers.ModelSerializer):
    truck_number = serializers.SerializerMethodField()

    def get_truck_number(self, instance):
        return instance.number

    class Meta:
        model = Truck
        fields = [
            'location', 
            'load_capacity',
            'truck_number',
        ]


class LocationSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Location
        fields = '__all__'


class ShipmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shipment
        fields = '__all__'


class ShipmentListSerializer(serializers.ModelSerializer):
    location_delivery_detail = LocationSerializer(read_only=True, source='location_delivery')
    location_pick_up_detail = LocationSerializer(read_only=True, source='location_pick_up')
    trucks_count = serializers.SerializerMethodField()

    def get_trucks_count(self, instance):
        #cache_trucks 
        trucks_cache_name = 'trucks_450'
        trucks_cache = cache.get(trucks_cache_name)
        if trucks_cache:
            trucks = trucks_cache
        else:
            trucks = get_distance_trucks(450, instance.location_pick_up)
            cache.set(trucks_cache_name, trucks, 60)
        return trucks

    class Meta:
        model = Shipment
        deep = 1
        fields = '__all__'


