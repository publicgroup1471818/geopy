from django.urls import path, include

from src.routers import truck_router, shipment_router

urlpatterns = [
    path('truck/', include(truck_router.urls)),
    path('shipment/', include(shipment_router.urls)),
]
