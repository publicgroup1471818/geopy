from django_filters import rest_framework as filters

from src.models import Shipment


class ShipmentFilter(filters.FilterSet):
    """Фильтр по весу."""
    
    weight = filters.NumericRangeFilter()

    class Meta:
        model = Shipment
        fields = ['weight']
    


