import string
from geopy import distance
from random import randint, choice

from django.db.utils import IntegrityError
from rest_framework.serializers import ValidationError

from src.models import Truck, Location, Shipment


def truck_id_generator(char=string.ascii_uppercase) -> str:
    """Формирует номер машины."""
    rand_num = str(randint(1000, 9999))
    return ''.join([rand_num,choice(char)])


def get_unique_truck_number() -> str | None:
    use_numbers = [truck.number for truck in Truck.objects.all()]
    return compare_numbers(use_numbers)


def compare_numbers(use_numbers: list[str]) -> str | None:
    """Сравнивает номер с использоваными."""
    number = truck_id_generator()
    if number in use_numbers:
        compare_numbers(use_numbers)
    else:
        return number


def create_random_trucks(
        trucks_quantiny: int,
        locations_zips: list[str]) -> None:
    """Создает модель машины."""
    try:
        for _ in range(int(trucks_quantiny)):
            new_truck = Truck.objects.create(
                number = get_unique_truck_number(),
                location = Location.objects.get(pk=choice(locations_zips)),
                load_capacity = randint(1, 1000),
            )
            new_truck.save()
    except TypeError as e:
        raise ValidationError(f'pk must convert to integer, error -> {e}')
    except ValueError as e:
        raise ValidationError(f'pk must convert to integer, error -> {e}')
    except IntegrityError:
        pass


def get_distance_trucks(distance_ml: int, cur_location: Location) -> int:
    """Сравнивает и возвращает кол-во машин в заданном расстоянии."""
    all_trucks = Truck.objects.all()
    trucks_count = 0
    for truck in all_trucks:
        if distance.distance(
            (cur_location.lat, cur_location.lng),
            (truck.location.lat, truck.location.lng)).miles <= distance_ml:
            trucks_count += 1
    return trucks_count


def get_all_trucks_for_shipment(cur_shipment: Shipment) -> dict:
    """Возвращает все машины и расстояния до них, для определенного груза."""
    trucks = Truck.objects.all().only('number', 'location')
    tmp_dict = {}
    for truck in trucks:
        tmp_dict[truck.number] = \
        distance.distance(
            (cur_shipment.location_pick_up.lat, cur_shipment.location_pick_up.lng),
            (truck.location.lat, truck.location.lng)).miles
    return tmp_dict
