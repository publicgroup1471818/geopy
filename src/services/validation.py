from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_weight(value: int) -> None:
    """Валидация веса."""
    if value < 1 or value > 1000:
        raise ValidationError(
            _('Значение должно быть в диапазоне 1-1000'),
            params={"value": value},
        )

def validate_truck_load_capacity(value: int) -> None:
    """Валидация загрузки машины."""
    if value < 1 or value > 1000:
        raise ValidationError(
            _('Значение должно быть в диапазоне 1-1000'),
            params={"value": value},
        )
