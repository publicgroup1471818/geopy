from django.db import models

from src.models.location import Location
from src.services.validation import validate_weight 


class Shipment(models.Model):

    class Meta:
        db_table = 'shipment'
        verbose_name = 'Груз'
        verbose_name_plural = 'Грузы'
        ordering = ['pk']

    location_pick_up = models.ForeignKey(
        Location,
        on_delete=models.PROTECT,
        null=True,
        verbose_name='Локация pick-up',
        related_name='location_pick_up',
    )
    location_delivery = models.ForeignKey(
        Location,
        on_delete=models.PROTECT,
        null=True,
        verbose_name='Локация delivery',
        related_name='location_delivery',
    )
    weight = models.IntegerField(
        verbose_name='Вес',
        validators=[validate_weight],
    )
    description = models.TextField(
        verbose_name='Описание',
    )

    def __str__(self):
        return f'Груз №{self.pk}'


