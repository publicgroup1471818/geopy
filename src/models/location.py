from django.db import models


class Location(models.Model):

    class Meta:
        db_table = 'location'
        verbose_name = 'Локация'
        verbose_name_plural = 'Локации'
        ordering = ['pk']

    zip = models.CharField(
        max_length=50,
        verbose_name='Zip код',
        primary_key=True,
    )
    city = models.CharField(
        max_length=100,
        blank=True, null=True
    )
    state_id = models.CharField(
        max_length=50,
        blank=True, null=True
    )
    state_name = models.CharField(
        max_length=100,
        blank=True, null=True
    )
    lat = models.CharField(
        max_length=255,
        blank=True, null=True
    )
    lng = models.CharField(
        max_length=255,
        blank=True, null=True
    )

    def __str__(self):
        return f'Локация №{self.pk}, широта: {self.lat}, долгота: {self.lng}'

