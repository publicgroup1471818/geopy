from django.db import models

from src.models.location import Location
from src.services.validation import validate_weight


class Truck(models.Model):

    class Meta:
        db_table = 'truck'
        verbose_name = 'Грузовик'
        verbose_name_plural = 'Грузовики'
        ordering = ['pk']

    number = models.CharField(
        max_length=255,
        primary_key=True,
        verbose_name='Уникальный номер'
    )
    location = models.ForeignKey(
        Location, on_delete=models.PROTECT,
        verbose_name='Текущая локация'
    )
    load_capacity = models.IntegerField(
        verbose_name='Грузоподъемность',
        validators=[validate_weight],
    )

    def __str__(self):
        return f'Грузовик №{self.number}'

