from django.contrib import admin

from src.models import Location, Truck, Shipment

admin.site.register(
    [ 
        Location, Truck, Shipment
    ]
)
        
