from django_filters.rest_framework import DjangoFilterBackend
from django.core.cache import cache

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from src.models import Truck, Location
from src.models.shipment import Shipment
from src.serializers import ShipmentListSerializer, \
    ShipmentSerializer, TruckSerializer
from src.services.filters import ShipmentFilter
from src.services.services import create_random_trucks, \
    get_all_trucks_for_shipment, get_unique_truck_number


class TruckModelViewSet(viewsets.ModelViewSet):
    queryset = Truck.objects.all()
    serializer_class = TruckSerializer
    http_method_names = ['get', 'post', 'patch', 'head']

    @action(detail=True, methods=['get'])
    def default_create_truck(self, request, pk=None) -> Response | None:
        if trucks_quantity := pk:
            locations_zips = [loc.zip for loc in Location.objects.all()]
            create_random_trucks(trucks_quantity, locations_zips)
            return Response(f'Создано {trucks_quantity} грузовиков.')

    def create(self, request, *args, **kwargs) -> Response:
        serializer = self.get_serializer(data=request.data) 
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['number'] = get_unique_truck_number()
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ShipmentModelViewSet(viewsets.ModelViewSet):
    queryset = Shipment.objects.all()
    serializer_class = ShipmentListSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ShipmentFilter
    
    def retrieve(self, request, pk, *args, **kwargs) -> Response:
        instance = Shipment.objects.get(pk=pk)
        serializer = ShipmentSerializer(instance)
        
        #cache_trucks 
        all_trucks_cache_name = 'all_trucks'
        all_trucks_cache = cache.get(all_trucks_cache_name)
        if all_trucks_cache:
            trucks = all_trucks_cache
        else:
            trucks = get_all_trucks_for_shipment(instance)
            cache.set(all_trucks_cache_name, trucks, 60)

        return Response({
                'shipment': serializer.data,
                'all_trucks': trucks
            }, status=status.HTTP_200_OK)

