from rest_framework import routers

from src.views import ShipmentModelViewSet, TruckModelViewSet

truck_router = routers.SimpleRouter()
truck_router.register(r'', TruckModelViewSet)

shipment_router = routers.SimpleRouter()
shipment_router.register(r'', ShipmentModelViewSet)

